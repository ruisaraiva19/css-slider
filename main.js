if (window.NodeList && !NodeList.prototype.forEach) {
  NodeList.prototype.forEach = function (callback, thisArg) {
    thisArg = thisArg || window;
    for (var i = 0; i < this.length; i++) {
      callback.call(thisArg, this[i], i, this);
    }
  };
}

var slider = document.querySelector('.slider');
var items = document.querySelectorAll('.slider__item');

function setWidth() {
  var width = 0;
  items.forEach(function (item) {
    return width += item.clientWidth;
  });
  items.forEach(function (item) {
    return slider.appendChild(item.cloneNode(true));
  });
  items.forEach(function (item) {
    return slider.appendChild(item.cloneNode(true));
  });
  slider.style.setProperty('width', width * 3 + 'px');
}

imagesLoaded(slider, function (instance) {
  setWidth();
});
